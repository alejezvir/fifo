package com.azvir.fifo.service;

public class NotEnoughParamsException extends RuntimeException {
    public NotEnoughParamsException() {
    }

    public NotEnoughParamsException(String message) {
        super(message);
    }
}
