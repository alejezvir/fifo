package com.azvir.fifo;

import com.azvir.fifo.domain.Delivery;

public interface Crud {

    void addProductToStore(String productName);

    void putToStore(String productName, Delivery delivery);
}
