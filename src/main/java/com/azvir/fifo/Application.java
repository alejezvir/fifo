package com.azvir.fifo;

import com.azvir.fifo.domain.*;
import com.azvir.fifo.repo.PhoneRepo;
import com.azvir.fifo.service.NotEnoughParamsException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.GarbageCollectorMXBean;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Application {
    public static void main(String[] args) throws Exception {
        Application application = new Application();
        Application.run();

    }

    private static void run() throws Exception {

        System.out.print("\nHELP or h - show instruction\n" +
                "NEWPRODUCT <String Name> \n" +
                "PURCHASE <String Name> <int Count> <int Cost> <Date dd.MM.yyyy> \n" +
                "DEMAND <String Name> <int Count> <int Cost> <Date dd.MM.yyyy> \n" +
                "SALESREPORT <String Name> <Date dd.MM.yyyy> \n");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command;

        Store store = Store.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        PhoneRepo phoneRepo = new PhoneRepo(store);

        while (!(command = reader.readLine()).equals("exit")) {

            String[] s = command.split(" ");


            switch (s[0].toUpperCase()) {

                case "NEWPRODUCT":
                    try {
                        if (2 != s.length) {
                            throw new NotEnoughParamsException();
                        }
                        phoneRepo.addProductToStore((String) s[1]);

                    } catch (NotEnoughParamsException ex) {
                        System.out.println("Count of parameters must be 2");
                    }
                    continue;

                case "PURCHASE":
                    Delivery purchase = new DeliveryPhone();
                    try {
                        if (5 != s.length) throw new NotEnoughParamsException();

                        if (Integer.parseInt(s[2]) <= 0 || Integer.parseInt(s[3]) <= 0)
                            throw new InvalidParameterException();

                        purchase.setCount(Integer.parseInt(s[2]));
                        purchase.setCost(Integer.parseInt(s[3]));
                        purchase.setDateOfDelivery(dateFormat.parse(s[4]));
                        purchase.setPurchaseOrDemand(false);
                        phoneRepo.putToStore(s[1], purchase);

                    } catch (InvalidParameterException ex) {
                        System.out.println("Parameters must be greater than zero");
                    } catch (NumberFormatException ex) {
                        System.out.println("Count of parameters must be 5");
                    } catch (ParseException ex) {
                        System.out.println("Fifth parameter must be date");
                    } catch (NotEnoughParamsException ex) {
                        System.out.println("Parameters must be Integer");
                    }

                    continue;

                case "DEMAND":
                    Delivery demand = new DeliveryPhone();
                    try {
                        if (5 != s.length) throw new NotEnoughParamsException();

                        if (Integer.parseInt(s[2]) <= 0 || Integer.parseInt(s[3]) <= 0)
                            throw new InvalidParameterException();

                        demand.setCost(Integer.parseInt(s[3]));
                        demand.setDateOfDelivery(dateFormat.parse(s[4]));
                        demand.setCount(Integer.parseInt(s[2]));
                        demand.setPurchaseOrDemand(true);
                        phoneRepo.putToStore(s[1], demand);

                    } catch (InvalidParameterException ex) {
                        System.out.println("Parameters must be greater than zero");
                    } catch (NumberFormatException ex) {
                        System.out.println("Third parameter must be Integer");
                    } catch (NotEnoughParamsException ex) {
                        System.out.println("Count of parameters must be 5");
                    } catch (ParseException ex) {
                        System.out.println("Parameters must be date");
                    }

                    continue;

                case "SALESREPORT":
                    Reports report = new ReportsOfPhone();
                    try {

                        if (3 != s.length) throw new NotEnoughParamsException();

                        System.out.println(report.calculate(s[1], dateFormat.parse(s[2]), store));

                    } catch (ParseException ex) {
                        System.out.println("Third parameter must be date");
                    } catch (NotEnoughParamsException ex) {
                        System.out.println("Count of parameters must be 3");
                    }
                    continue;

                default:
                    System.out.print("\nHELP or h - show instruction\n" +
                            "NEWPRODUCT <String Name> \n" +
                            "PURCHASE <String Name> <int Count> <int Cost> <Date dd.MM.yyyy> \n" +
                            "DEMAND <String Name> <int Count> <int Cost> <Date dd.MM.yyyy> \n" +
                            "SALESREPORT <String Name> <Date dd.MM.yyyy> \n");

            }
        }
    }
}
