package com.azvir.fifo.repo;

import com.azvir.fifo.Crud;
import com.azvir.fifo.domain.Delivery;
import com.azvir.fifo.domain.Store;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PhoneRepo implements Crud {
    private Store store;

    public PhoneRepo(Store store) {
         this.store = store;
    }

    public void addProductToStore(String productName) {

        Map<String, List<Delivery>> storeCollection = store.getStoreCollection();
        if (storeCollection.containsKey(productName)) {
            System.out.println("Error");
        } else {
            storeCollection.put(productName, new ArrayList<Delivery>());
            System.out.println("OK");
        }
    }

    public void putToStore(String productName, Delivery delivery) {
        Map<String, List<Delivery>> storeCollection = this.store.getStoreCollection();

        if (storeCollection.containsKey(productName)) {

            for (Map.Entry<String, List<Delivery>> pair : storeCollection.entrySet()) {
                if (pair.getKey().equals(productName)) {
                    pair.getValue().add(delivery);
                    System.out.println("OK");
                }
            }
        } else {
            System.out.println("Error. Product doesn't contain");
        }
    }


}
