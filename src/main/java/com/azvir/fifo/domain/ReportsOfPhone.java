package com.azvir.fifo.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportsOfPhone extends Reports {

    @Override
    public int calculate(String productName, Date date,Store store) {

        createReportofProduct(productName, date, store);
        int costForDemand = 0;
        int clearEarning = 0;
        int countOfDemandProducts = 0;
        List<Delivery> list = new ArrayList<>();

        for ( Delivery delivery : getListOfDelivery()) {
       // for ( Delivery delivery : listOfDelivery) {
            if (delivery.getDateOfDelivery().before(date)
                    || delivery.getDateOfDelivery().equals(date)) {
                list.add(delivery);
            }
        }

        for (Delivery deliveryPhone : list) {
            if (deliveryPhone.isPurchaseOrDemand()) {
                costForDemand += deliveryPhone.getCost();
                countOfDemandProducts += deliveryPhone.getCount();
            }
        }
        clearEarning = costForDemand * countOfDemandProducts;

        for (Delivery deliveryPhoneofPurchace:list
        ) {
            if (!deliveryPhoneofPurchace.isPurchaseOrDemand()){
                if (clearEarning >= 0 && countOfDemandProducts >= 0) {
                    clearEarning -= deliveryPhoneofPurchace.getCost();
                    countOfDemandProducts -= deliveryPhoneofPurchace.getCount();
                } else {
                    clearEarning -= countOfDemandProducts* deliveryPhoneofPurchace.getCost();
                }
            }

        }

        return clearEarning;
    }


}
