package com.azvir.fifo.domain.Products;

import java.util.Date;

public class TV extends Product {
    private String type = String.valueOf(Product.ProductType.TV);

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getModel() {
        return super.getModel();
    }

    @Override
    public void setModel(String model) {
        super.setModel(model);
    }

    @Override
    public String getSerialNumber() {
        return super.getSerialNumber();
    }

    @Override
    public void setSerialNumber(String serialNumber) {
        super.setSerialNumber(serialNumber);
    }

    @Override
    public Date getDateOfManufacture() {
        return super.getDateOfManufacture();
    }

    @Override
    public void setDateOfManufacture(Date dateOfManufacture) {
        super.setDateOfManufacture(dateOfManufacture);
    }
}
