package com.azvir.fifo.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store {
    private  Map<String, List<Delivery>> storeCollection = new HashMap<String, List<Delivery>>();
    private static Store store;
    private Store() {
    }

    public static Store  getInstance() {

        if (store == null) {
            return new Store();
        }

        return store;
    }

    public  Map<String, List<Delivery>> getStoreCollection() {
        return storeCollection;
    }
}
