package com.azvir.fifo.domain;

import com.azvir.fifo.domain.Products.Product;

import java.util.Date;

public class Delivery {
    private int count;
    private int cost;
    private Date dateOfDelivery;
    private boolean purchaseOrDemand;
    private Product product;

    public Delivery() {
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public boolean isPurchaseOrDemand() {
        return purchaseOrDemand;
    }

    public void setPurchaseOrDemand(boolean purchaseOrDemand) {
        this.purchaseOrDemand = purchaseOrDemand;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Date getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(Date dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }
}
