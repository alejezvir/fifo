package com.azvir.fifo.domain;

import java.util.Date;

public class DeliveryPhone extends Delivery {

    public DeliveryPhone() {
    }

    @Override
    public boolean isPurchaseOrDemand() {
        return super.isPurchaseOrDemand();
    }

    @Override
    public void setPurchaseOrDemand(boolean purchaseOrDemand) {
        super.setPurchaseOrDemand(purchaseOrDemand);
    }

    @Override
    public void setCount(int count) {
        super.setCount(count);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public int getCost() {
        return super.getCost();
    }

    @Override
    public void setCost(int cost) {
        super.setCost(cost);
    }

    @Override
    public Date getDateOfDelivery() {
        return super.getDateOfDelivery();
    }

    @Override
    public void setDateOfDelivery(Date dateOfDelivery) {
        super.setDateOfDelivery(dateOfDelivery);
    }
}
